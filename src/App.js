import React from "react";

/*  
  Provider je komponenta koja spaja React i Redux
  Sve komponente wrappovane unutar Providera-a će moći da pristupe Redux state-u
  Kao obavezan argument mora da mu se dodeli redux store koji smo definisali u folderu store
*/

import { Provider } from "react-redux";
import Counter from "./components/Counter";
import store from "./store/store";

const App = () => {
  return (
    <Provider store={store}>
      <div className="App">
        App
        <Counter />
      </div>
    </Provider>
  );
}

export default App;
