import React from "react";
import { connect } from "react-redux";
import { updateCounter } from "../store/thunks"

const Counter = ({ counter, updateCounter }) => {
   
    return (
        <React.Fragment>
            <h1>Number: {counter}</h1>
            <div>
                <button onClick={() => updateCounter("increment", 5)}>+</button>
                <button onClick={() => updateCounter("decrement", 1)}>-</button>
            </div>
        </React.Fragment>
    )
}

// Mora da vrati objekat, spaja redux state sa komponentom
const mapStateToProps = (state) => {
    return {
       counter: state.counter
    }
};

// Mora da vrati objekat, spaja redux akcije sa komponentom
const mapDispatchToProps = (dispatch) => {
    return {
        updateCounter: (type, value) => dispatch(updateCounter(type, value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);