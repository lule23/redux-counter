/* 
    Ovde se definišu tipovi akcija, na osnovu kojih će reducer znati šta treba da odradi sa state-om
    Konvekcija je da se tipovi akcija pišu velikim slovima, i da njihova vrednost bude string
    Koriste se varijable kako ne bi došlo do dupliranja naziva akcija ili gramatičkih grešaka prilikom
    provere akcije u reducer-u
*/

const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
const DECREMENT_COUNTER = 'DECEMENT_COUNTER';

export { INCREMENT_COUNTER, DECREMENT_COUNTER }