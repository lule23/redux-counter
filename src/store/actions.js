import { INCREMENT_COUNTER, DECREMENT_COUNTER } from "./actionTypes";

/* 
    Ovde se definišu action creators
    To su funkcije koje kao rezultat vraćaju objekat koji predstavlja redux akciju
    Svaki objekat obavezno mora da ima property type jer se akcija po tome prepoznaje u reduceru
    sve ostale informacije se stavljaju u property payload.

    Action creator obavezno mora da vrati objekat
*/

const incrementCounter = (value) => {
    return  {
        type: INCREMENT_COUNTER,
        payload: value
    }
}

const decrementCounter = (value) => {
    return  {
        type: DECREMENT_COUNTER,
        payload: value
    }
}

export { incrementCounter, decrementCounter }