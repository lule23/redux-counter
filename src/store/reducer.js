import { INCREMENT_COUNTER, DECREMENT_COUNTER } from "./actionTypes";

const initialState = {
    counter: 0
}


/* 
    Reducer funkcija prima 2 argumenta = state sa kojim radi i akciju koja je pozvana
    Akcija je ustvari redux akcija koja se poziva sa dispatch funkcijom, a koja je definisana unutar actionCreator funkcije
    u folderu actions.

    Za svaku akciju se definiše CASE u kojem uvek treba da se vrati novi state, nikako da se radi sa postojećim.
    Ako želimo da se sačuva stari sadržaj state-a onda je obavezno da se on kopira prilikom vraćanja novog.
*/

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT_COUNTER: 
            return {
                ...state, 
                counter: state.counter + action.payload
            }

        case DECREMENT_COUNTER:
            return {
                ...state,
                counter: state.counter - action.payload
            }
            
        default:
            return state;
    }

}

export default rootReducer;