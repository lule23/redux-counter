import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducer";

/* 
   createStore akcija prima 3 argumenta, od kojih je jedan obavezan.
   Obavezan argument je prvi, a to je reducer.

   Reducer mora da bude samo jedan.
   Ako koristimo više reducera, onda se oni moraju iskombinovati u jedan pomoću funkcije combineReducers
   koja se dobija u reduxu.

   Drugi argument je početni inicijalni state, taj argument se veoma retko dodeljuje, jer se najčešće 
   početni state definiše untuar reducer-a.

   Umesto tog arugmenta, drugi može da se zameni sa trećim, a to su dodatna unapređenja za redux store.
   Jedno od unapređenja je dodela middleware funkcija kroz koje prolaze akcije dok ne dođu do reducera.

   Pored toga, moguće je spajanje sa redux dev tools ekstenzijom iz google chrome-a.
*/

const store = createStore(rootReducer, compose(applyMiddleware(thunk), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));

export default store;