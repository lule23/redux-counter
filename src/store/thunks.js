import { incrementCounter, decrementCounter } from "./actions";

/* 
    Ovde se nalaze funkcije za thunk middleware. 
    Thunk funkcija je funkcija koja vraća funkciju.
    Funkcija koju vraća dobija od redux-a 2 argumenta: dispatch i getState

    Dispatch se koristi da pokrene klasične redux akcije, a getState da nam vrati aktuelan redux state.

    Svaka thunk funkcija može da se pokrene putem dispatch funkcije.
*/

const updateCounter = (type, value) => {
    return (dispatch, getState) => {
        const state = getState();
        console.log(state.counter)
        if(type === "increment") {
            dispatch(incrementCounter(value));
        } else if(type === "decrement") {
            dispatch(decrementCounter(value))
        }
    }
}

export { updateCounter }